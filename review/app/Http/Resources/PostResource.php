<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\PostHasTag;


class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' =>  $this->title,
            'description' =>  $this->description,
            'url' =>  $this->url,
            'rating' =>  $this->rating,
            'source' =>  $this->source,
            'created_at' => (string) $this->created_at,
            'user' => $this->user,
            'comment' => $this->comment,
            'image' => $this->post_img,
            'tag' => PostHasTagResource::collection(PostHasTag::where('post_id', $this->id)->get()),
            

        ];
    }
}
