<?php

namespace App\Http\Controllers;

use App\PostImg;
use Illuminate\Http\Request;

class PostImgController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PostImg  $postImg
     * @return \Illuminate\Http\Response
     */
    public function show(PostImg $postImg)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PostImg  $postImg
     * @return \Illuminate\Http\Response
     */
    public function edit(PostImg $postImg)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PostImg  $postImg
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PostImg $postImg)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PostImg  $postImg
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostImg $postImg)
    {
        //
    }
}
