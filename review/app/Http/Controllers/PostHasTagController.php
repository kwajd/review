<?php

namespace App\Http\Controllers;

use App\PostHasTag;
use Illuminate\Http\Request;

class PostHasTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PostHasTag  $postHasTag
     * @return \Illuminate\Http\Response
     */
    public function show(PostHasTag $postHasTag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PostHasTag  $postHasTag
     * @return \Illuminate\Http\Response
     */
    public function edit(PostHasTag $postHasTag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PostHasTag  $postHasTag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PostHasTag $postHasTag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PostHasTag  $postHasTag
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostHasTag $postHasTag)
    {
        //
    }
}
