<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'title', 'description', 'url', 'rating', 'source'
    ];

    /**
    * Get user for this post
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
    * Get user for this post
    */
    public function comment()
    {
        return $this->hasMany('App\Comment');
    }

    /**
    * Get user for this post
    */
    public function favourites()
    {
        return $this->hasMany('App\Favourites');
    }

    /**
    * Get user for this post
    */
    public function post_img()
    {
        return $this->hasMany('App\PostImg');
    }

    /**
    * Get user for this post
    */
    public function post_has_tag()
    {
        return $this->hasMany('App\PostHasTag');
    }
}
