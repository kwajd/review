<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <link rel="shortcut icon" href="{{{ asset('logo.png') }}}">
    <link rel="stylesheet" href="css/style.css">

    <title>Review</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet">

    <!-- Styles -->
    <style>
    </style>
</head>

<body>

    <div class="nav-bar">
        @include('layout.nav')
    </div>

    <div class="content">@yield('content')</div>

    <div class="footer">
        @include('layout.footer')
    </div>




</body>

</html>