<style>
  .navbar {
    background-color: white;
    box-shadow: 0 2px 3px 0 rgba(0, 0, 0, .16);
    margin: 0;
  }

  .navbar a {
    float: left;
    color: #f2f2f2;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    font-size: 17px;
    color: white;

  }

  .navbar .left:hover {
    background-color: #FF7F00;
  }

  .navbar .right:hover {
    background-color: #FF7F00;
  }

  .navbar .left {
    background-color: #FF9D12;
    float: left !important;
  }

  .navbar .right {
    background-color: #FF9D12;
    float: right !important;
  }

  .navbar .center {
    margin: auto;
  }
</style>

<div class="navbar">
  <div class="left"><a href="/posts">Posts</a></div>
  <div class="center"><a class="active" href="/"><img src="logo.png" width="5%" alt="logo"></a></div>
  <div class="right"><a href="#contact">Contact</a>






  </div>