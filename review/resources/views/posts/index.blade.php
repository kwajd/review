@extends('layout')

@section('content')

<link rel="stylesheet" href="css/posts.css">

<div class="posts-container">
    <div class="posts">
        @foreach ($posts as $post)
        <div class="card">
            <div class="card-image"></div>
            <div class="card-text">
                <span class="date">{{$post->user->name}}</span>
                <span class="date">4 days ago</span>
                <h2>{{$post->title}}</h2>
                <p>{{$post->description}}</p>
            </div>


            <div class="card-stats">
                <div class="stat">
                    <div class="value">4</div>
                    <div class="type">follows</div>
                </div>
                <div class="stat">
                    <div class="value">83</div>
                    <div class="type">views</div>
                </div>
                <div class="stat">
                    <div class="value">12</div>
                    <div class="type">comments</div>
                </div>
            </div>
        </div>

        @endforeach
    </div>

    <div class="pagination">
        {{ $posts->links() }}
    </div>


</div>






@endsection